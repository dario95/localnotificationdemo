# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

LocalNotificationDemo


Se realizó la implementación de notificaciones locales en iOS. Apple ha cambiado desde iOS 10. En versiones anteriores, las notificaciones eran parte de UIKit. 

1. Primero es necesario crear un proyecto, en el cuál irá un botón.

2. Las notificaciones son un nuevo framework. por lo que será necesatio importar "UserNotifications" en nuestro view controller.
3. Antes de realizar cualquier notificación se debe verificar que existe el permiso para usarlas. El objeto UNUserNotificacionCenter.current() permitirá realizar una solicitud de autorización.
4. El permiso será almacenado en una variable global que luego será verificada en el botón. Dentro del botón utilizaremos el objeto UNMutableNotificationContect el cual almacenará el contenido de la notificación.
5. "categoryUdentifier" es una propiedad que nos ayuda a asociar acciones a la notificación.
6. Luego crearemos un trigger para que la notificación sea presentada luego de 10 segundos.
7. Finalmente tomaremos el trigger y el contenido, y agregaremos un string para nombrar la solicitud. Este identificador es importante ya que previene al sistema disparar la misma notificación muchas veces.También permite el control sobre la misma.
